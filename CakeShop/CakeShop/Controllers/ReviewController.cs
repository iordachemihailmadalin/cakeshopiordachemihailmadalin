﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CakeShop.BindingModels;
using CakeShop.Data;
using CakeShop.Models;
using CakeShop.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CakeShop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReviewController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public ReviewController(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Review
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReviewViewModel>>> GetReviews(int? cakeId = null)
        {
            var reviews = new List<Review>();
            var reviewViewModels = new List<ReviewViewModel>();
            try
            {
                if (cakeId == null)
                {
                    reviews = await _context.Reviews.Include(review => review.Cake).ToListAsync();
                }
                else
                {
                    reviews = await _context.Reviews.Include(review => review.Cake).Where(c => c.CakeId == cakeId).ToListAsync();
                }

                reviewViewModels = _mapper.Map<List<ReviewViewModel>>(reviews);

            }
            catch (Exception ex)
            {

            }

            return reviewViewModels;
        }

        // GET: api/Review/5
        [HttpGet("{id}")]
        public ActionResult<ReviewViewModel> GetReview(int id)
        {
            try
            {
                var review = _context.Reviews.Include(c => c.Cake).ToListAsync();
                var reviewViewModel = _mapper.Map<ReviewViewModel>(review);

                if (review == null)
                {
                    return NotFound();
                }

                return reviewViewModel;
            }
            catch (Exception ex)
            {

            }

            return NotFound();
        }

        // POST: api/Review
        [HttpPost]
        [Route("PostReview")]
        public async Task<ActionResult<Review>> PostReview([FromForm] ReviewBindingModel reviewBindingModel)
        {
            try
            {
                var review = _mapper.Map<Review>(reviewBindingModel);

                _context.Reviews.Add(review);
                await _context.SaveChangesAsync();

                return CreatedAtAction(nameof(GetReview), new { id = review.ReviewId }, review);
            }
            catch (Exception ex)
            {

            }

            return BadRequest();
        }

        // DELETE: api/Review/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteReview([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var review = await _context.Reviews.FindAsync(id);
            if (review == null)
            {
                return NotFound();
            }

            _context.Reviews.Remove(review);
            await _context.SaveChangesAsync();

            return Ok(review);
        }

        private bool ReviewExists(int id)
        {
            return _context.Reviews.Any(e => e.ReviewId == id);
        }
    }
}