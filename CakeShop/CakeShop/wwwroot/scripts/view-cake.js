﻿const cakeControllerUri = "/api/cake/";
const reviewControllerUri = "/api/review/";

$(document).ready(function () {
    var cakeId = getUrlVars()["cakeid"];
    getCake(cakeId);
    getReviews(cakeId);
});

// Read a page's GET URL variables and return them as an associative array.
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function displayCakeDetails(cake) {
    $('#cake-name').html(cake.name);
    $('#cake-price').html(cake.price + ' lei');
    $('#cake-description').html(cake.description);
    $('#cake-category').html(cake.category);
    $('#side-bar-cake-category').text(cake.category);
    $('#side-bar-cake-category').attr("href", "/homepage/index.html");
    $('#cake-image').css('max-height', '400px');
    $('#cake-image').attr("src", cake.base64Image);
}

function getCake(cakeId) {
    $.ajax({
        type: "GET",
        url: cakeControllerUri + cakeId,
        cache: false,
        success: function (data) {
            displayCakeDetails(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Something went wrong when getting the cakes!");
        }
    });
}

function displayReviews(reviews) {
    $("#reviews").empty();

    $.each(reviews, function (key, review) {
        $("#reviews").append('<h2>Rating: ' + review.rating + '/5 points.</h2><p>' + review.comment + '</p><small class="text-muted"> Posted by ' + review.emailAddress + '</small><hr>');
    });

}

function getReviews(cakeId) {
    $.ajax({
        type: "GET",
        url: reviewControllerUri + cakeId,
        cache: false,
        success: function (data) {
            displayReviews(data)
            alert("Got the reviews!");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Something went wrong when getting the reviews!");
        }
    });
}

function addReview() {
    var formData = new FormData();

    formData.append("rating", $("#rating").val());
    formData.append("comment", $("#comment").val());
    formData.append("emailAddress", $("#email").val());
    formData.append("cakeId", getUrlVars()["cakeid"]);

    $.ajax({
        type: "POST",
        url: reviewControllerUri + "PostReview",
        processData: false,
        contentType: false,
        data: formData,
        mimeType: "multipart/form-data",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Something went wrong when trying to add the review!");
        },
        success: function (result) {
            alert("Review was posted successfully!");
            getReviews(cakeId);
        }
    });

    return false;
}

$(".add-review-form").on("submit", function () {
    addReview();

    return false;
});